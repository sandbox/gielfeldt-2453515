<?php

/**
 * @file
 * Definition of Drupal\configuration\Config\VariableConfiguration.
 */

namespace Drupal\configuration_extra\Config;

use Drupal\configuration\Config\VariableConfiguration;
use Drupal\configuration\Utils\ConfigIteratorSettings;

class VariableExtraConfiguration extends VariableConfiguration {
  /**
   * Overrides Drupal\configuration\Config\Configuration::isActive().
   */
  public static function isActive() {
    return TRUE;
  }

  /**
   * Overrides Drupal\configuration\Config\Configuration::getComponentHumanName().
   */
  static public function getComponentHumanName($component, $plural = FALSE) {
    return $plural ? t('Variables (extra)') : t('Variable (extra)');
  }

  /**
   * Overrides Drupal\configuration\Config\Configuration::findRequiredModules().
   */
  public function findRequiredModules() {
    $this->addToModules('configuration_extra');
    $this->addToModules('configuration');
    $this->addToModules('xautoload');
  }
}
