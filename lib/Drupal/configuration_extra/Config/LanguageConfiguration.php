<?php

/**
 * @file
 * Definition of Drupal\configuration\Config\VariableConfiguration.
 */

namespace Drupal\configuration_extra\Config;

use Drupal\configuration\Config\Configuration;
use Drupal\configuration\Utils\ConfigIteratorSettings;

class LanguageConfiguration extends Configuration {
  /**
   * Overrides Drupal\configuration\Config\Configuration::getComponentHumanName().
   */
  static public function getComponentHumanName($component, $plural = FALSE) {
    return $plural ? t('Languages') : t('Language');
  }

  /**
   * Overrides Drupal\configuration\Config\Configuration::getComponent().
   */
  public function getComponent() {
    return 'language';
  }

  /**
   * Overrides Drupal\configuration\Config\Configuration::supportedComponents().
   */
  static public function supportedComponents() {
    return array('language');
  }

  /**
   * Overrides Drupal\configuration\Config\Configuration::getAllIdentifiers().
   */
  public static function getAllIdentifiers($component) {
    $languages = language_list();
    $return = array();
    foreach ($languages as $langcode => $language) {
      $return["language__$langcode"] = $language->name;
    }
    $return['default__language'] = 'Default language';
    return $return;
  }

  /**
   * Overrides Drupal\configuration\Config\Configuration::findRequiredModules().
   */
  public function findRequiredModules() {
    $this->addToModules('locale');
    $this->addToModules('configuration_language');
    $this->addToModules('configuration');
    $this->addToModules('xautoload');
  }

  /**
   * Implements Drupal\configuration\Config\Configuration::prepareBuild().
   */
  protected function prepareBuild() {
    list($type, $name) = explode('__', $this->getIdentifier(), 2);

    switch ($type) {
      case 'language':
        return $this->prepareBuildLanguage($name);
      case 'default':
        return $this->prepareBuildDefaultLanguage();
    }
  }

  private function getLanguage($langcode) {
    $languages = language_list();
    $language = $languages[$langcode];
    $language->javascript = '';
    return $language;
  }

  private function prepareBuildLanguage($langcode) {
    $this->data = $this->getLanguage($langcode);
    return $this;
  }

  private function prepareBuildDefaultLanguage() {
    $this->data = language_default();
    $this->data->javascript = '';
    return $this;
  }

  private function saveToActiveStoreLanguage() {
    $language = $this->getData();

    db_merge('languages')
      ->key(array('language' => $language->language))
      ->fields((array) $language)
      ->execute();

    $count = 0;
    $languages = language_list();
    foreach ($languages as $lang) {
      if ($lang->enabled) {
        $count++;
      }
    }
    variable_set('language_count', $count);

    // Kill the static cache in language_list().
    drupal_static_reset('language_list');

    // Force JavaScript translation file creation for the newly added language.
    _locale_invalidate_js($language->language);

    module_invoke_all('multilingual_settings_changed');
  }


  /**
   * Overrides Drupal\configuration\Config\Configuration::saveToActiveStore().
   */
  public function saveToActiveStore(ConfigIteratorSettings &$settings) {
    list($type, $name) = explode('__', $this->getIdentifier(), 2);
    switch ($type) {
      case 'language':
        $this->saveToActiveStoreLanguage();
        break;

      case 'default':
        variable_set('language_default', $this->getData());
        break;
    }
  }
}
