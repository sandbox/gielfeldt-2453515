<?php

/**
 * @file
 * Definition of Drupal\configuration\Config\VariableConfiguration.
 */

namespace Drupal\configuration_extra\Config;

use Drupal\configuration\Config\Configuration;
use Drupal\configuration\Utils\ConfigIteratorSettings;

class ModuleConfiguration extends Configuration {
  /**
   * Overrides Drupal\configuration\Config\Configuration::getComponentHumanName().
   */
  static public function getComponentHumanName($component, $plural = FALSE) {
    return $plural ? t('Modules') : t('Module');
  }

  /**
   * Overrides Drupal\configuration\Config\Configuration::getComponent().
   */
  public function getComponent() {
    return 'module';
  }

  /**
   * Overrides Drupal\configuration\Config\Configuration::supportedComponents().
   */
  static public function supportedComponents() {
    return array('module');
  }

  /**
   * Overrides Drupal\configuration\Config\Configuration::getAllIdentifiers().
   */
  public static function getAllIdentifiers($component) {
    $modules = system_list('module_enabled');
    $return = array();
    foreach ($modules as $module) {
      if (!preg_match('/\.module$/', $module->filename)) {
        continue;
      }
      $version = $module->info['version'] ? $module->info['version'] : 'Unknown';
      $return[$module->name] = $module->info['name'] . " ($module->name-$version)";
    }
    return $return;
  }

  /**
   * Overrides Drupal\configuration\Config\Configuration::findRequiredModules().
   */
  public function findRequiredModules() {
    $this->addToModules('configuration_extra');
    $this->addToModules('configuration');
    $this->addToModules('xautoload');
  }

  /**
   * Implements Drupal\configuration\Config\Configuration::prepareBuild().
   */
  protected function prepareBuild() {
    $modules = system_list('module_enabled');
    $this->data = array(
      'name' => $this->getIdentifier(),
      'version' => $modules[$this->getIdentifier()]->info['version'],
    );
    return $this;
  }

  /**
   * Overrides Drupal\configuration\Config\Configuration::saveToActiveStore().
   */
  public function saveToActiveStore(ConfigIteratorSettings &$settings) {
    module_enable(array($this->getIdentifier()));
  }
}
