<?php

/**
 * @file
 * Definition of Drupal\configuration_extra\Config\ThemeConfiguration.
 */

namespace Drupal\configuration_extra\Config;

use Drupal\configuration\Config\Configuration;
use Drupal\configuration\Config\ConfigurationManagement;
use Drupal\configuration\Utils\ConfigIteratorSettings;

class ThemeConfiguration extends Configuration {
  /**
   * Overrides Drupal\configuration\Config\Configuration::getComponentHumanName().
   */
  static public function getComponentHumanName($component, $plural = FALSE) {
    return $plural ? t('Themes') : t('Theme');
  }

  /**
   * Overrides Drupal\configuration\Config\Configuration::getComponent().
   */
  public function getComponent() {
    return 'theme';
  }

  /**
   * Overrides Drupal\configuration\Config\Configuration::supportedComponents().
   */
  static public function supportedComponents() {
    return array('theme');
  }

  /**
   * Overrides Drupal\configuration\Config\Configuration::getAllIdentifiers().
   */
  public static function getAllIdentifiers($component) {
    $return = array();
    foreach (list_themes() as $theme) {
      if ($theme->status) {
        $return[$theme->name] = $theme->name . ': ' . $theme->info['name'];
      }
    }
    return $return;
  }

  /**
   * Overrides Drupal\configuration\Config\Configuration::findRequiredModules().
   */
  public function findRequiredModules() {
    $this->addToModules('configuration_extra');
    $this->addToModules('configuration');
    $this->addToModules('xautoload');
  }

  /**
   * Implements Drupal\configuration\Config\Configuration::prepareBuild().
   */
  protected function prepareBuild() {
    $this->data = $this->getIdentifier();
    return $this;
  }

  /**
   * Overrides Drupal\configuration\Config\Configuration::saveToActiveStore().
   */
  public function saveToActiveStore(ConfigIteratorSettings &$settings) {
    theme_enable(array($this->getData()));
  }

  /**
   * Overrides Drupal\configuration\Config\Configuration::alterDependencies().
   */
  public static function alterDependencies(Configuration $config) {
    $config_ids = array();

    if ($config->getComponent() == 'variable' && !$config->broken) {
      switch ($config->getIdentifier()) {
        case 'theme_default':
          if ($theme = variable_get('theme_default', '')) {
            $config_ids[] = 'theme.' . $theme;
          }
          break;

        case 'admin_theme':
          if ($theme = variable_get('admin_theme', '')) {
            $config_ids[] = 'theme.' . $theme;
          }
          break;
      }
    }
    foreach ($config_ids as $config_id) {
      $theme = ConfigurationManagement::createConfigurationInstance($config_id);
      $theme->build();
      $config->addToDependencies($theme);
    }
  }
}
