<?php

/**
 * @file
 * configuration_extra.drush.inc
 * Let you perform configuration actions from the console.
 */

use Drupal\configuration\Config\ConfigurationManagement;

/**
 * Implements of hook_drush_help()
 */
function configuration_extra_drush_help($section) {
  switch ($section) {
    case 'drush:dtm':
      return dt("Download tracked modules.");
  }
}

/**
 * Implements of hook_drush_command().
 */
function configuration_extra_drush_command() {
  $items = array();
  $items['download-tracked-modules'] = array(
    'description' => 'Download tracked modules.',
    'aliases' => array('dtm'),
    'examples' => array(
      'drush dtm' => 'Download tracked modules',
    ),
  );
  return $items;
}

/**
 * List of all components and its identifiers.
 */
function drush_configuration_extra_download_tracked_modules() {
  //$modules = system_list('module_enabled');
  $modules = system_rebuild_module_data();
  //print_r($modules);
  //return;
  // $configuration = ConfigurationManagement::createConfigurationInstance($component);

  //get tracked file
  $tracked = ConfigurationManagement::trackedConfigurations(FALSE);
  foreach ($tracked as $component => $info) {
    //var_dump($component);
    //check status
    $configuration = ConfigurationManagement::createConfigurationInstance($component);
    if ($configuration->getComponent() === 'module') {
      $name = $configuration->getIdentifier();
      $info = $configuration->loadFromStorage()->getData();
      // var_dump($info);
      if (empty($modules[$name])) {
        print "DOWNLOAD: $name\n";
        drush_invoke('pm-download', array($name, '--yes'));
      }
      else{
        $datastore_version = _mangle_version($info['version']);
        $activestore_version = _mangle_version($modules[$name]->info['version']);
        if (version_compare($activestore_version, $datastore_version, '<')) {
          print "VERSION MISMATCH: $name (have:$activestore_version - need:$datastore_version)\n";
          drush_invoke_process('@self', 'pm-download', array($name . '-' . $datastore_version), array('--yes'));
        }
        else {
          print "ALREADY HAVE: $name (have:$activestore_version - need:$datastore_version)\n";
        }
      }
    }
  }
}

function _mangle_version($version) {
  if (preg_match('/(\d+)\.(\d+|x)-(\d+)\.(\d+|x).*-dev$/', $version, $matches)) {
    list(,$dmaj, $dmin, $mmaj, $mmin) = $matches;
    return "$dmaj.$dmin-$mmaj.x-dev";
  }
  return $version;
}

function _mangle_test() {
  $version = '7.x-2.0-alpha4+23-dev';
  var_dump($version, _mangle_version($version));
}

function _compare_test() {
  $version1 = '7.x-2.0-beta1';
  $version2 = '7.x-2.x-dev';
  var_dump($version, _mangle_version($version));
}
